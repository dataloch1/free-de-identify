  Feature: Redact misspelt patient name
    Because the information is freely entered
    The redaction software
    Must be able to cope with spell variations
    and typos

    Scenario Outline: Fuzzy matching
      Given that the patient's name is <firstname> <lastname>
      And a report of "Lorem ipsum dolor <firstname_error> <lastname_error> sit amet"
      When the report is redacted
      Then the report is redacted to "Lorem ipsum dolor [[NAME]] [[NAME]] sit amet"
      
    Examples: Different spellings
        | firstname | firstname_error | lastname | lastname_error |
        | Ian   | Iain  | Doe | Doe |
        | Linda | Lynda | Doe | Doe |
    Examples: Typos
        | firstname | firstname_error | lastname | lastname_error |
        | Paul  | Pual  | Mitchell | Mitchel |
        | Paul  | Pual  | Mitchell | Micthell |
        