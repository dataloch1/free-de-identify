Feature: Redact pathology numbers
  In order to be allow free text to be provided to the PPZ
  DataLoch as information processor
  Must ensure that the data is had the identifiers removed,
  not all identifiers form part of the patient demographic

    Scenario: Redact long pathology number      
      Given a report of "Lorem ipsum dolor UB012345K/22 sit amet"
      When the report is redacted
      Then the report is redacted to "Lorem ipsum dolor [[PATHOLOGY_NO]] sit amet"

    Scenario: Redact normal pathology number      
      Given a report of "Lorem ipsum dolor UB012345/22 sit amet"
      When the report is redacted
      Then the report is redacted to "Lorem ipsum dolor [[PATHOLOGY_NO]] sit amet"

    Scenario: Redact short form pathology number
      Given a report of "Lorem ipsum dolor UB2345/24 sit amet"
      When the report is redacted
      Then the report is redacted to "Lorem ipsum dolor [[PATHOLOGY_NO]] sit amet"

    Scenario: Redact normal pathology number without year
      Given a report of "Lorem ipsum dolor UB012345 sit amet"
      When the report is redacted
      Then the report is redacted to "Lorem ipsum dolor [[PATHOLOGY_NO]] sit amet"

    