Feature: Redact relations in report
  In order to be allow free text to be provided to the PPZ
  DataLoch as information processor
  Must redact any relation names that are mentioned in
  a report

  Scenario: Daughter's full name in report
    Given the standard stop word list
    And a report of "Lorem ipsum dolor daughter Jane Doe consectetur adipiscing elit"
    When the report is redacted
    Then the report is redacted to "Lorem ipsum dolor daughter [[RELATION]] [[RELATION]] consectetur adipiscing elit"

  Scenario: Son's first name in report
    Given the standard stop word list
    And a report of "Lorem ipsum dolor son John consectetur adipiscing elit"
    When the report is redacted
    Then the report is redacted to "Lorem ipsum dolor son [[RELATION]] consectetur adipiscing elit"
