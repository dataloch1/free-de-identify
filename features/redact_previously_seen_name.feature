Feature: Redact previously seen relations in report
  In order to be allow free text to be provided to the PPZ
  DataLoch as information processor
  Must redact any relation names that are mentioned in
  a report

  Scenario: Previously seen son in report
  Given that son Paul was previously seen in a report
  When a report with Paul in it
  Then Paul is redacted from the report

  Scenario: Long report with son in report
  Given a long report with the son's name in it
  When redacted
  Then all the names are removed
  