  Feature: Redact other information
    In order to be allow free text to be provided to the PPZ
    DataLoch as information processor
    Should redact unnecessary personal information

    Scenario: Report with email address
      Given a report of "Lorem ipsum dolor a.user@localhost.com sit amet"
      When the report is redacted
      Then the report is redacted to "Lorem ipsum dolor [[EMAIL]] sit amet"

    Scenario: Report with GMC no
      Given a report of "Lorem ipsum dolor GMC No 123456789 sit amet"
      When the report is redacted
      Then the report is redacted to "Lorem ipsum dolor [[GMC_NUMBER]] sit amet"

    Scenario: Report with GMC number
      Given a report of "Lorem ipsum dolor GMC: 123456789 sit amet"
      When the report is redacted
      Then the report is redacted to "Lorem ipsum dolor [[GMC_NUMBER]] sit amet"      