from behave import *
from redact.healthcare_professional import HealthcareProfessional
from redact.patient import Patient
from redact.email_address import EMailAddress
from redact.gmc_number import GMCNumber
from redact.telephone import TelephoneNumber
from redact.ward import Ward
from redact.relation import Relations

@given(u'a report of "{text}"')
def step_impl(context, text):
  context.line = text


@when(u'the report is redacted')
def step_impl(context):
  context.redacted = context.line
  if hasattr(context, 'patient'):
    p = Patient(name=context.patient['first'] + " " + context.patient['last'], dob=context.patient['dob'], uhpi=context.patient['uhpi'])
    context.redacted = p.remove_name(context.redacted)
    context.redacted = p.remove_uhpi(context.redacted)
    context.redacted = p.remove_chis(context.redacted)
    
  p = Patient()
  context.redacted = p.remove_pathology_no(context.redacted)
  e = EMailAddress()
  context.redacted = e.remove(context.redacted)
  t = TelephoneNumber()
  context.redacted = t.remove(context.redacted)
  g = GMCNumber()
  context.redacted = g.remove(context.redacted)
  w = Ward()
  context.redacted = w.remove(context.redacted)
  c = HealthcareProfessional()
  context.redacted = c.remove(context.redacted)

  if hasattr(context, 'stop_list'):
    wi = Relations({ "Daughter": context.stop_list})
    si = Relations({ "Son": context.stop_list})

    context.redacted = wi.remove(context.redacted)
    context.redacted = si.remove(context.redacted)


@then(u'the report is redacted to "{text}"')
def step_impl(context, text):
  r = context.redacted
  assert r == text

@given(u'that son Paul was previously seen in a report')
def step_impl(context):
  context.test_report_action = Relations({ "Son": ["and", "therefore", "for", "there"]})
  context.test_report_action.remove("this has the son Paul in it")


@when(u'a report with Paul in it')
def step_impl(context):
  context.redacted_text = context.test_report_action.remove("This has Paul in it again and Paul test")


@then(u'Paul is redacted from the report')
def step_impl(context):
  assert context.redacted_text == "This has [[RELATION]] in it again and [[RELATION]] test"

@given(u'a long report with the son\'s name in it')
def step_impl(context):
  context.test_report_action = Relations({ "Son": ["and", "therefore", "for", "there"]}) 


@when(u'redacted')
def step_impl(context):
  context.redacted_text = context.test_report_action.remove("this has the son Paul in it.  It has many words, some of which contain the name Paul again.  This should catch any Paul left over from the orginal redaction of the name Paul.")


@then(u'all the names are removed')
def step_impl(context):
  assert context.redacted_text.find("Paul") == -1
