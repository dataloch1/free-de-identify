from behave import *
from matchers.levenshtein_matcher import LevenshteinMatcher
from matchers.beider_morse_matcher import BeiderMorseMatcher

@given(u'the name "{type1}"')
def step_impl(context, type1):
    context.type1 = [type1.strip()]

@when(u'compared to "{type2}"')
def step_impl(context, type2):
    context.type2 = type2.strip()

@then(u'the two should match via Levenshtein')
def step_impl(context):
  cmp = LevenshteinMatcher(context.type1)
  assert cmp.find(context.type2) != -1

@then(u'the two should match via Beider-Morse')
def step_impl(context):
  cmp = BeiderMorseMatcher(context.type1)
  assert cmp.find(context.type2) != -1
