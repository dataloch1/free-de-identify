from behave import *
import datetime

@given(u'that the patient\'s name is {first} {last}')
def step_impl(context, first, last):
    context.patient = {'first': first, 'last': last, 'dob': datetime.date.today(), 'uhpi': 'anystringvalue'}

@given(u'that the patient\'s DOB is {dob}')
def step_impl(context, dob):
    context.patient = {'first': 'jane', 'last': 'last', 'dob': datetime.datetime.strptime(dob,r"%d/%m/%Y"), 'uhpi': 'anystringvalue'}

@given(u'that the patient\'s UHPI is {uhpi}')
def step_impl(context, uhpi):
    context.patient = {'first': 'jane', 'last': 'last', 'dob': datetime.date.today(), 'uhpi': uhpi}        
    