from behave import *

@given(u'the standard stop word list')
def step_impl(context):
    context.stop_list = ["Abx", "Approx", "Australia", "Ax", "consectetur"]