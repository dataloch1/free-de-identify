  Feature: Redact other information
    Because the information is freely entered
    The redaction software
    Must be able to cope with spell variations
    and typos

    Scenario Outline: Fuzzy matching
      Given the name "<type1>"
      When compared to "<type2>"
      Then the two should match via Beider-Morse

    Examples: Different spellings
        | type1 | type2 | 
        | Ian   | Iain  |
        | Linda | Lynda |

    Examples: Typos
        | type1 | type2 | 
        | Paul  | Pual  |
        | Mitchell | Mitchel | 
        