Feature: Redact ward round report
  In order to be allow free text to be provided to the PPZ
  DataLoch as information processor
  Should redacted ward reports that mention clinical staff names
  and ward numbers

    Scenario: Redact doctor's name
      Given a report of "WR Dr Paul Mitchell <<NR>> Lorem ipsum dolor sit amet"
      When the report is redacted
      Then the report is redacted to "WR [[HEALTHCARE_PROFESSIONAL]] <<NR>> Lorem ipsum dolor sit amet"

    Scenario: Redact ward number
      Given a report of "Lorem ipsum ward 123 dolor sit amet"
      When the report is redacted
      Then the report is redacted to "Lorem ipsum [[WARD]] dolor sit amet"

    Scenario: Redact ward name
      Given a report of "Lorem ipsum Rowan ward dolor sit amet"
      When the report is redacted
      Then the report is redacted to "Lorem ipsum [[WARD]] dolor sit amet"         
      