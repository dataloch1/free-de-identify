Feature: Report with patient details
  In order to be allow free text to be provided to the PPZ
  DataLoch as information processor
  Must ensure that the data is had the identifiers removed
  
    Scenario: Redact patient name
      Given that the patient's name is Jane Doe
      And a report of "Lorem ipsum dolor Jane Doe sit amet"
      When the report is redacted
      Then the report is redacted to "Lorem ipsum dolor [[NAME]] sit amet"

    Scenario: Redact patient's first name
      Given that the patient's name is Jane Doe
      And a report of "Lorem ipsum dolor Jane sit amet"
      When the report is redacted
      Then the report is redacted to "Lorem ipsum dolor [[NAME]] sit amet"

    Scenario: Redact patient's surname name
      Given that the patient's name is Jane Doe
      And a report of "Lorem ipsum dolor Ms Doe sit amet"
      When the report is redacted
      Then the report is redacted to "Lorem ipsum dolor Ms [[NAME]] sit amet"

    Scenario: Redact patient CHI
      Given that the patient's DOB is 12/12/1912
      And a report of "Lorem ipsum dolor 1212121234 sit amet"
      When the report is redacted
      Then the report is redacted to "Lorem ipsum dolor [[CHI]] sit amet"
      
    Scenario: Redact patient UHPI
      Given that the patient's UHPI is 5001231234A
      And a report of "Lorem ipsum dolor 5001231234A sit amet"
      When the report is redacted
      Then the report is redacted to "Lorem ipsum dolor [[UHPI]] sit amet"