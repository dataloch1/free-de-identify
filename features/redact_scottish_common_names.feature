  Feature: Redact common Scottish names
    In Scotland name with Mac and Mc are common
    However, these name can be typed and misspelt
    in many variations.
    The software should be able to find these
    so that they can be redacted.

      Scenario Outline: Fuzzy matching
      Given the name "<type1>"
      When compared to "<type2>"
      Then the two should match via Levenshtein

    Examples: Different styles
        | type1     | type2 | 
        | McDuck    | MacDuck  |
        | McDuck    | MCDUCK |
        | McDuck    | Mc Duck |
        | McDuck    | MDuck |
        | McDuck    | MCuck |
