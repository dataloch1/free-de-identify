from operator import truediv


try:
    import re2 as re
except ImportError:
    import re

class Date:
  def __init__(self, date, sub="[[DATE]]") -> None:
    self._date_regexp = re.compile("(?P<day>\d{1,2})[\./-]?(?P<month>\d{1,2})[\./-]?(?P<year>\d{2,4})")
    self.date = date
    self.sub = sub

  def remove(self, line):
    match = self._date_regexp.search(line)
    if match and self._match_group_matches_date(match):
      line = self._date_regexp.sub(self.sub, line)

    return line

  def _match_group_matches_date(self, match):
    day   = int(match.group("day"))
    month = int(match.group("month"))
    year  = int(match.group("year"))

    if (day == self.date.day) and (month == self.date.month) and self._year_matches(year):
      return True

    return False
  
  def _year_matches(self, year) -> bool:
    years = [year, year + 1900, year + 2000]

    if self.date.year in years:
      return True

    return False

      