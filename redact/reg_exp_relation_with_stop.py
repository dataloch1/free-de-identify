try:
    import re2 as re
except ImportError:
    import re

from matchers.space_tokenizer import SpaceTokenizer

class RegExpRelationWithStop:
  def __init__(self,  relation, sub="RELATION", stop=[]) -> None:
    rel_reg = f"(?i)(?P<relation>{re.escape(relation)})(?-i)"    
    self.regexp = re.compile(rel_reg + r"\s?[(]?[DMrs]*\s(?P<name1>[A-Z][A-Za-z,']+)(?P<name2>(?:\s)[A-Z][A-Za-z'.,]+)?\s?")
    self.sub = sub
    self.stop = list(map(str.lower, stop))
    self.list_found_relations = []

  def reg_matcher(self, line):
    match = self.regexp.search(line)
    if match:
      if self._match_in_stop(match.group("name1")):
        return line

      self.list_found_relations.append(match.group("name1").strip())
      if self._match_in_stop(match.group("name2")):
        return self.regexp.sub(f'{match.group("relation")} [[{self.sub}]] {match.group("name2")} ', line)

      if match.lastgroup == "name2":
        self.list_found_relations.append(match.group("name2").strip())

      return self.regexp.sub(f'{match.group("relation")} [[{self.sub}]] ', line)

    return line

  def remove_found_relations(self, wordTokens):    
    strip = lambda string: ''.join(filter(str.isalnum, string.lower()))
    for i in range(len(wordTokens)):
      for relation in list(filter(None, self.list_found_relations)):
        if (strip(relation) == strip(wordTokens[i])):
          wordTokens[i] = self._ireplace(relation, f"[[{self.sub}]]", wordTokens[i])

    return wordTokens

  def _match_in_stop(self, string) -> bool:
    if string == None:
      return False

    return string.strip().lower() in self.stop

  def _ireplace(self, old, new, text) -> str:
    idx = 0
    while idx < len(text):
        index_l = text.lower().find(old.lower(), idx)
        if index_l == -1:
            return text
        text = text[:index_l] + new + text[index_l + len(old):]
        idx = index_l + len(new) 
    return text