try:
    import re2 as re
except ImportError:
    import re

class TelephoneNumber:
  def __init__(self, sub="[[TELEPHONE]]") -> None:
    self.__sub = sub
    self.__regexp = re.compile(r"(((\+44\s?\d{4}|\(?0\d{4}\)?)\s?\d{3}\s?\d{3})|((\+44\s?\d{3}|\(?0\d{3}\)?)\s?\d{3}\s?\d{4})|((\+44\s?\d{2}|\(?0\d{2}\)?)\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?")

  def remove(self, line):
    return self.__regexp.sub(self.__sub, line)
    