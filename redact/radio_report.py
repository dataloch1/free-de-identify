try:
    import re2 as re
except ImportError:
    import re

class RadioReport:
  def __init__(self, sub="[[HEALTHCARE_PROFESSIONAL]]") -> None:
    self.__sub = sub
    self.__reported_by = re.compile(r"Reported by(.+)")
    self.__report_by = re.compile(r"Report by(.+)")
    self.__checked_by = re.compile(r"Checked by(.+)")
    self.__overseen_by = re.compile(r"Overseen by(.+)")
    self.__scanned_by = re.compile(r"Scanned by(.+)")
    self.__hcpc = re.compile(r"<<NL>>[A-Za-z\s]+<<NL>>(?:<<NL>>)?[A-Za-z\s]+<<NL>>(?:<<NL>>)?HCPC(.+)")
    self.__line = re.compile(r"[^>]+HCPC(.+)")
    self.__titles = [re.compile(r"[^<>]+<<NL>>Consultant Radiologist<<NL>>.+"),  re.compile(r"[^<>]+<<NL>>Advanced Practitioner Radiographer[\.]?<<NL>>.+")]

  def remove(self, line) -> str:
    line = self.__reported_by.sub(f"Reported by: {self.__sub}", line)
    line = self.__report_by.sub(f"Report by {self.__sub}", line)
    line = self.__checked_by.sub(f"Checked by {self.__sub}", line)
    line = self.__overseen_by.sub(f"Overseen by {self.__sub}", line)
    line = self.__scanned_by.sub(f"Scanned by {self.__sub}", line)
    if ("HCPC" in line):
      line = self.hcpc_redact(line)
    line = self.redact_by_title(line)
    return line

  def hcpc_redact(self, line) -> str:
    line = self.__hcpc.sub(self.__sub, line)
    if ("HCPC" in line):
      line = self.__line.sub(self.__sub, line)
    return line

  def redact_by_title(self, line) -> str:
    for reg in self.__titles:
      line = reg.sub(self.__sub, line)

    return line