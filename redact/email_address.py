try:
    import re2 as re
except ImportError:
    import re

class EMailAddress:
  def remove(self, line):
    r = re.compile(r"[A-Za-z0-9\.]+@[A-Za-z0-9\.]+")
    return r.sub("[[EMAIL]]", line)
    