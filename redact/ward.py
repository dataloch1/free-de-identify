try:
    import re2 as re
except ImportError:
    import re
from sys import flags
import configuration

class Ward:
  def __init__(self) -> None:
      self.wards = configuration.Configuration.current_configuration().load_wards()

  def remove(self, line) -> str:
    for ward_reg in self.wards:
      r = re.compile(ward_reg, flags=re.IGNORECASE)
      line = r.sub("[[WARD]]", line)

    return line

  