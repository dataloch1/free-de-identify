try:
    import re2 as re
except ImportError:
    import re

class HealthcareProfessional:
  reports = ['WR', 'R/V', 'Ward Round', 'D/w']
  dr_regexp = re.compile(r"[\"]?(?:[Dd][Rr])+?[\.]?\s?[A-Z][a-zA-Z]*(?:\s[A-Z][a-zA-Z]+)?")
  name_only = re.compile(r"[A-Z][a-zA-Z]*(?:\s[A-Z][a-zA-Z]+)")

  def __init__(self, sub="[[HEALTHCARE_PROFESSIONAL]]") -> None:
    grades = ['FY\d',  'IMT\d', 'CMT\d', 'CT\d', 'GPST\d', 'ST\d', 'SpR', 'LAS\sST\d', 'OT', 'S[\\\/]?N']
    titles = ['[Dd][Rr][\.]?\s', '[Mm][Rr][\.]?\s', '[Mm][Ss][\.]?\s', '[Ss][\\\/]?[Nn][\.]?\s']

    self.__sub = sub
    self.__reg_grades = list(map(lambda x: re.compile('(([A-Z][A-za-z]*\s?)+)' + f"[(]?{x}[)]?"), grades))
    self.__reg_titles = list(map(lambda x: re.compile('\\b' + x + '([A-Z][A-za-z]*\s?)+'), titles))

  def remove(self, line):
    for report in HealthcareProfessional.reports:
      if line.lower().startswith(report.lower()):
        if re.search(r"[Dd][Rr][\.]?\s", line):
          return HealthcareProfessional.dr_regexp.sub(self.__sub, line)
        else:
          return line[:len(report)] + HealthcareProfessional.name_only.sub(self.__sub, line[len(report):])

    line = self._redact_with_title(line)
    line = self._redact_with_grade(line)
    return self._redact_grade_with_nl(line)

  def _redact_with_title(self, line):
    for reg in self.__reg_titles:
      if reg.search(line):
        line = reg.sub(self.__sub, line)

    return line

  def _redact_with_grade(self, line):
    for reg in self.__reg_grades:
      if reg.search(line):
        line = reg.sub(self.__sub, line)

    return line

  def _redact_grade_with_nl(self, line):
    newline = '<<NL>>'
    striped = line.replace(newline, ' ')
    for reg in self.__reg_grades:
      matched = reg.search(striped)
      if matched:
        name = matched.group(1).strip()
        line = line.replace(name, self.__sub)

    return line
