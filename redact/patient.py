from calendar import month
from unicodedata import name
from matchers.levenshtein_matcher import LevenshteinMatcher
from matchers.beider_morse_matcher import BeiderMorseMatcher
from matchers.jaro_winkler_matcher import JaroWinklerMatcher
from matchers.space_tokenizer import SpaceTokenizer
from enum import Enum, auto
from redact.date import Date

import datetime
try:
    import re2 as re
except ImportError:
    import re

## BeiderMorseMatcher
class Matchers(Enum):
  LEVENSHTEIN = auto()
  BEIDEN_MORSE = auto()
  JARO_WINKLER = auto()

class Patient:
  def __init__(self, name="", aliases=None, dob=datetime.date.today(), uhpi=[], matchers=Matchers.JARO_WINKLER) -> None:
    self.name = name
    self.aliases = aliases
    self.dob = dob
    self.matchers = matchers
    if uhpi and isinstance(uhpi, str):
      uhpi = [uhpi]
      
    self.uhpis = uhpi

  def remove(self, line) -> str:
    r = self.remove_chis(line)
    r = self.remove_name(r)
    r = self.remove_uhpi(r)
    r = self.remove_pathology_no(r)
    return self.remove_dob(r)
    
  def remove_dob(self, str):
    d = Date(self.dob, sub="[[DOB]]")
    return d.remove(str)
  
  def remove_name(self, str) -> str:
    result = str
    names = self.name.split(' ')
    result = self._ireplace(f"{self.name}", "[[NAME]]", result)
    if len(names) > 1:
      result = self._ireplace(f"{names[0]} {names[-1]}", "[[NAME]]", result)

    for part in names:
      result = self._ireplace(f"{part}", "[[NAME]]", result)      

    tk = SpaceTokenizer(result).getWordTokens()
    if self.matchers is Matchers.JARO_WINKLER:
      cmp = JaroWinklerMatcher(tk)
    elif self.matchers is Matchers.LEVENSHTEIN:
      cmp = LevenshteinMatcher(tk)
    else:
      cmp = BeiderMorseMatcher(tk)

    for part in names:      
      i = cmp.find(part)
      if ( i > 0):
        fname = tk[i]
        result = self._ireplace(fname, "[[NAME]]", result)

    return result

  def remove_uhpi(self, line) -> str:
    for uhpi in self.uhpis:
      line = self._ireplace(uhpi, '[[UHPI]]', line)

    line = re.compile("\d{9}[A-Za-z]").sub("[[SUSPICIOUS_NUMBER]]", line)
    return line

  def remove_chis(self, str) -> str:
    match = re.search(self._chi_regexp(), str)
    if match and self._matches_dob(match):
      str = re.sub(self._chi_regexp(), "[[CHI]]", str)

    str = re.sub(self._chi_regexp(), "[[OTHER_CHI]]", str)
    str = re.sub(r"\d{10}", "[[SUSPICIOUS_NUMBER]]", str)

    return str

  def remove_pathology_no(self, str) -> str:
    return re.sub(self._pathology_no_regexp(), "[[PATHOLOGY_NO]]", str)

  def _matches_dob(self, reg_match) -> bool:
    return reg_match.group(1) == '%02d' % self.dob.day
    
  def _chi_regexp(self):
    return re.compile(r"(0[1-9]|[12]\d|3[01])(0[1-9]|1[0-2])(\d{2})(\d{4})")

  def _pathology_no_regexp(self):
    return re.compile(r"U[A-Z]\d{3,6}[A-Z]?[\\/]?\d{2}?")

  def _2_digit_year(self):
    substract = 2000 if self.dob.year > 1999 else 1900

    return self.dob.year - substract

  def _ireplace(self, old, new, text) -> str:
    idx = 0
    while idx < len(text):
        index_l = text.lower().find(old.lower(), idx)
        if index_l == -1:
            return text
        text = text[:index_l] + new + text[index_l + len(old):]
        idx = index_l + len(new) 
    return text