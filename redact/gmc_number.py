try:
    import re2 as re
except ImportError:
    import re

class GMCNumber:
  def remove(self, line) -> str:
    r = re.compile(r'GMC\s?[N]?[Oo]?[:]?\s?\d+')

    return r.sub("[[GMC_NUMBER]]", line)
    