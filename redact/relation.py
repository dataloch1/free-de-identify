from redact.reg_exp_relation_with_stop import RegExpRelationWithStop
from matchers.space_tokenizer import SpaceTokenizer

class Relations:
  def __init__(self, relations) -> None:
    self.actions = []
    for k, v in relations.items():
      self.actions.append(RegExpRelationWithStop(k, stop=v))
      
  def remove(self, line):
    wordTokens = SpaceTokenizer(line).getWordTokens()
    for worker in self.actions:
      worker.reg_matcher(line)

    for worker in self.actions:
      wordTokens = worker.remove_found_relations(wordTokens)

    return ' '.join(wordTokens)
