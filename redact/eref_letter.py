try:
    import re2 as re
except ImportError:
    import re

class ErefLetter:
  def __init__(self, sub="[[HEALTHCARE_PROFESSIONAL]]") -> None:
    self.__sub = sub
    self.__reg_sincerely = re.compile(r"Yours sincerely(?:.+)")
    self.__reg_faithfully = re.compile(r"Yours faithfully(?:.+)")
    self.__reg_ref_from = re.compile(r"Referral from:(?:.+)Main referral")

  def remove(self, line):
    line = self.__reg_faithfully.sub(f"Yours faithfully{self.__sub}", line)
    line = self.__reg_sincerely.sub(f"Yours sincerely{self.__sub}", line)
    line = self.__reg_ref_from.sub(f"Referral from: {self.__sub}<<NL>><<NL>>Main referral", line)
    return line