import io

class Document:

  def __init__(self, file_name) -> None:
      self.file_name = file_name

  def load(self):
    try:
      self.file = io.FileIO(self.file_name, 'r')
      return True
    except FileNotFoundError:
      print(f'File not found: {self.file_name}')
      return False
  def steam(self):
    return self.file
    