import json
import io
import pathlib

class Configuration:
  def __init__(self, config_file) -> None:
    self.configuration = json.load(io.FileIO(config_file))

  @staticmethod  
  def current_configuration():
    path = pathlib.Path(__file__).parent.resolve()
    return Configuration(f'{path}/config/free_de_identify-conf.json')

  def load_hospitals(self):
    return self.configuration['hospitals']

  def load_wards(self):
    return self.configuration['wards']

  def load_relations(self):
    return self.configuration['relations']
    
