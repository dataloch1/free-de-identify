import re

from configuration import Configuration

class HospitalNames:
  def __init__(self) -> None:
      self.replacements = Configuration.current_configuration().load_hospitals()

  def normalize(self, line) -> str:
    for k, v in self.replacements.items():
      for fullname in v:
        line = self._ireplace(fullname, f"{k}", line)

    return line

  def _ireplace(self, old, repl, text) -> str:
    return re.sub('(?i)'+re.escape(old), lambda m: repl, text)
