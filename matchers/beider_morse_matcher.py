from abydos.phonetic import BeiderMorse

class BeiderMorseMatcher:
  def __init__(self, tokens) -> None:
    self.__tokens = tokens
    self.__engine = BeiderMorse('english', 'gen', 'approx', False)
    self.__min = 3

  def find(self, word):
    list = self.__get_encoded()
    encoded = self.__engine.encode(word).split(' ')

    for i in range(len(list)):
      if (len(self.__tokens[i]) >= self.__min) and (self.__compare_length(list[i]['encoded'], encoded) >= 2):
        return i

    return -1

  def __compare_length(self, list1, list2) -> int:
    return len(list(set(list1) & set(list2)))

  def __get_encoded(self):
    if hasattr(self, '__encoded'):
      return self.__encoded

    mapper = lambda token: { "word": token, "encoded": self.__engine.encode(token).split(' ')}
    self.__encoded = list(map(mapper, self.__tokens))

    return self.__encoded