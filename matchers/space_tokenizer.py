try:
    import re2 as re
except ImportError:
    import re

class SpaceTokenizer:
  def __init__(self, sentence) -> None:
    self.__sentence = sentence
    self.__tokens = []

  def getWordTokens(self):
    return self.__getTokens()

  def __getTokens(self):
    if hasattr(self, '__tokens'):
      return self.__tokens

    nls = re.split("(<<NL>>)", self.__sentence)
    for item in nls:      
      self.__tokens.extend(item.split(' '))

    return self.__tokens