from abydos.distance import JaroWinkler

class JaroWinklerMatcher:
  def __init__(self, tokens) -> None:
      self.__tokens = tokens
      self.__cmp = JaroWinkler(long_strings=True)
      self.__min = 2
      self.__match = 0.901

  def find(self, word) -> int:    
    if len(word) <= self.__min: 
      return -1

    for i in range(len(self.__tokens)):
      if len(self.__tokens[i]) <= self.__min:
        continue
      
      token_word = self.__tokens[i].casefold()
      if token_word in ["mac", "mc"]:
        token_word = self.__tokens[i].casefold() + self.__tokens[i+1].casefold()
        
      if self.__cmp.sim(word.casefold(), token_word) >= self.__match:
        return i

    return -1
    
    