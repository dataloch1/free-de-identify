from abydos.distance import Levenshtein

class LevenshteinMatcher:
  def __init__(self, tokens) -> None:
      self.__tokens = tokens
      self.__cmp = Levenshtein()
      self.__min = 3

  def find(self, word) -> int:    
    if len(word) > 4: 
      max = 2 
    else:
      max = 1

    for i in range(len(self.__tokens)):
      token_word = self.__tokens[i].casefold()
      if token_word in ["mac", "mc"]:
        token_word = self.__tokens[i].casefold() + self.__tokens[i+1].casefold()
        
      if (len(token_word) >= self.__min) and (self.__cmp.dist_abs(word.casefold(), token_word) <= max):
        return i

    return -1
    
    