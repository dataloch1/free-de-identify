import spacy

class Tokenizer:
  def __init__(self, sentence) -> None:
    self.__sentence = sentence

  def getWordTokens(self):
    tokens = self.__getTokens()
    return list(map(lambda token: token.text, tokens))

  def __getTokens(self):
    if hasattr(self, '__tokens'):
      return self.__tokens

    nlp = spacy.load("en_core_web_sm")
    self.__tokens = nlp(self.__sentence)

    return self.__tokens