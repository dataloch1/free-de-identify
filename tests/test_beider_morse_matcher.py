from matchers.beider_morse_matcher import BeiderMorseMatcher

import pytest

class TestBeiderMorseMatcher:
  @pytest.fixture
  def sentence1(self):
    return f"Lorem ipsum dolor sit amet Lynda Doe consectetur adipiscing elit"

  @pytest.fixture
  def sentence2(self):
    return f"Lorem ipsum dolor sit amet Mat Doe consectetur adipiscing elit"

  @pytest.fixture
  def sentence3(self):
    return f"Lorem ipsum dolor sit amet Clare Doe consectetur adipiscing elit"


  def test_name1(self, sentence1):
    bm = BeiderMorseMatcher(sentence1.split(' '))
    assert bm.find('Linda') == 5

  def test_name2(self, sentence2):
    bm = BeiderMorseMatcher(sentence2.split(' '))
    assert bm.find('Matt') == 5 

  def test_name3(self, sentence3):
    bm = BeiderMorseMatcher(sentence3.split(' '))
    assert bm.find('Claire') == 5 

  def test_not_found(self, sentence3):
    bm = BeiderMorseMatcher(sentence3.split(' '))
    assert bm.find('Fred') == -1
