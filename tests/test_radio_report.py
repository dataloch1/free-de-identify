from redact.radio_report import RadioReport
import pytest

class TestRadioReport:
  def test_report_by_redact(self):
    sentence = "Report by Dr P Mitchell additional text"
    expected = "Report by [[HEALTHCARE_PROFESSIONAL]]"

    e = RadioReport()
    r = e.remove(sentence)

    assert r == expected

  def test_reported_by_redact(self):
    sentence = "Reported by: Dr P Mitchell additional text"
    expected = "Reported by: [[HEALTHCARE_PROFESSIONAL]]"

    e = RadioReport()
    r = e.remove(sentence)

    assert r == expected

  def test_checked_by_redact(self):
    sentence = "Checked by Dr P Mitchell additional text"
    expected = "Checked by [[HEALTHCARE_PROFESSIONAL]]"

    e = RadioReport()
    r = e.remove(sentence)

    assert r == expected

  def test_overseen_by_redact(self):
    sentence = "Overseen by Dr P Mitchell additional text"
    expected = "Overseen by [[HEALTHCARE_PROFESSIONAL]]"

    e = RadioReport()
    r = e.remove(sentence)

    assert r == expected

  def test_scanned_by_redact(self):
    sentence = "Scanned by Dr P Mitchell additional text"
    expected = "Scanned by [[HEALTHCARE_PROFESSIONAL]]"

    e = RadioReport()
    r = e.remove(sentence)

    assert r == expected

  def test_check_hcpc_line1(self):
    sentence = "Additional<<NL>>Text<<NL>><<NL>>Paul Mitchell<<NL>>Advanced Practitioner Radiographer<<NL>><<NL>>HCPC No. RA1234"
    expected = "Additional<<NL>>Text<<NL>>[[HEALTHCARE_PROFESSIONAL]]"

    e = RadioReport()
    r = e.remove(sentence)

    assert r == expected     


  def test_check_hcpc_line2(self):
    sentence = "Additional<<NL>>Text<<NL>><<NL>>Paul Mitchell<<NL>>Advanced Practitioner Radiographer<<NL>>HCPC No. RA1234"
    expected = "Additional<<NL>>Text<<NL>>[[HEALTHCARE_PROFESSIONAL]]"

    e = RadioReport()
    r = e.remove(sentence)

    assert r == expected  

  def test_check_hcpc_redact_line3(self):
    sentence = "Additional<<NL>>Text<<NL>>Paul Mitchell, Advanced Practitioner Radiographer, HCPC No. RA1234"
    expected = "Additional<<NL>>Text<<NL>>[[HEALTHCARE_PROFESSIONAL]]"

    e = RadioReport()
    r = e.remove(sentence)

    assert r == expected

  def test_redact_by_job_title1(self):
    sentence = "<<NL>>P Mitchell<<NL>>Consultant Radiologist<<NL>>paul.d.mitchell@ed.ac.uk"
    expected = "<<NL>>[[HEALTHCARE_PROFESSIONAL]]"

    e = RadioReport()
    r = e.remove(sentence)

    assert r == expected

  def test_redact_by_job_title2(self):
    sentence = "<<NL>>P Mitchell<<NL>>Advanced Practitioner Radiographer<<NL>>paul.d.mitchell@ed.ac.uk"
    expected = "<<NL>>[[HEALTHCARE_PROFESSIONAL]]"

    e = RadioReport()
    r = e.remove(sentence)

    assert r == expected

  def test_redact_by_job_title3(self):
    sentence = "<<NL>>P Mitchell<<NL>>Advanced Practitioner Radiographer.<<NL>>paul.d.mitchell@ed.ac.uk"
    expected = "<<NL>>[[HEALTHCARE_PROFESSIONAL]]"

    e = RadioReport()
    r = e.remove(sentence)

    assert r == expected
