from buffer import Buffer
import io

def test_import_from_file():
  f = io.BytesIO(b"This is a test")
  b = Buffer()
  b.load_from_file(f)
  assert b.buffer() == "This is a test"
