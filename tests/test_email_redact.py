from redact.email_address import EMailAddress
import pytest
import re

class TestEmailRedact:
  @pytest.fixture
  def sentence_with(self):
    def _sentence_with(pathology_no):
      return f"Lorem ipsum dolor sit amet {pathology_no} consectetur adipiscing elit"

    return _sentence_with

  def test_email_format_1(self, sentence_with):
    e = EMailAddress()
    r = e.remove(sentence_with('abcd@localhost.com'))

    assert "[[EMAIL]]" in r

  def test_email_format_2(self, sentence_with):
    e = EMailAddress()
    r = e.remove(sentence_with('abcd123@localhost.com'))

    assert "[[EMAIL]]" in r

  def test_email_format_3(self, sentence_with):
    e = EMailAddress()
    r = e.remove(sentence_with('abcd_123@localhost.com'))

    assert "[[EMAIL]]" in r

  def test_email_format_4(self, sentence_with):
    e = EMailAddress()
    r = e.remove(sentence_with('abcd-123@localhost.com'))

    assert "[[EMAIL]]" in r

  def test_email_format_5(self, sentence_with):
    e = EMailAddress()
    r = e.remove(sentence_with('abcd@localhost.co.uk'))

    assert "[[EMAIL]]" in r

  def test_email_format_6(self, sentence_with):
    e = EMailAddress()
    r = e.remove(sentence_with('abcd123@localhost.co.uk'))

    assert "[[EMAIL]]" in r

  def test_email_format_7(self, sentence_with):
    e = EMailAddress()
    r = e.remove(sentence_with('abcd_123@localhost.co.uk'))

    assert "[[EMAIL]]" in r

  def test_email_format_8(self, sentence_with):
    e = EMailAddress()
    r = e.remove(sentence_with('abcd-123@localhost.co.uk'))

    assert "[[EMAIL]]" in r