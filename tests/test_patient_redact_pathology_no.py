from datetime import datetime
from redact.patient import Patient

import pytest
import re

class TestPatientPathologyNo:
  @pytest.fixture
  def patient_under_test(self) -> Patient:
    return Patient(name="Jane Elizabeth Doe")

  @pytest.fixture
  def sentence_with(self):
    def _sentence_with(pathology_no):
      return f"Lorem ipsum dolor sit amet {pathology_no} consectetur adipiscing elit"

    return _sentence_with

  def test_pathology_full(self, sentence_with, patient_under_test):
    r = patient_under_test.remove_pathology_no(sentence_with("UB000456K/12"))

    assert "[[PATHOLOGY_NO]]" in r

  def test_pathology_with_check(self, sentence_with, patient_under_test):
    r = patient_under_test.remove_pathology_no(sentence_with("UB000456K/12"))

    assert "[[PATHOLOGY_NO]]" in r

  def test_pathology_short(self, sentence_with, patient_under_test):
    r = patient_under_test.remove_pathology_no(sentence_with("UB456/12"))

    assert "[[PATHOLOGY_NO]]" in r