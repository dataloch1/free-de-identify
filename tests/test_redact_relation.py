from redact.relation import Relations
import pytest

class TestRelationRedaction:
  @pytest.fixture
  def sentence1(self):
    return f"his brother Mike and sister Clare"

  @pytest.fixture
  def sentence2(self):
    return f"his Brother Mike and Sister Clare"

  @pytest.fixture
  def relations(self):
    return { "brother": ["and", "therefore", "for", "there"], "sister": ["Friday"] }

  def test_name_redaction1(self, sentence1, relations):
    test = Relations(relations)
    result = test.remove(sentence1).strip()

    assert result == "his brother [[RELATION]] and sister [[RELATION]]"

  def test_name_redaction2(self, sentence2, relations):
    test = Relations(relations)
    result = test.remove(sentence2).strip()

    assert result == "his Brother [[RELATION]] and Sister [[RELATION]]"    

  def test_name_whole_word_only(self, relations):
    sentence = "his brother Mike and sister Clare<<NL>>Community Mental Health Team therefore this is a mikely test Community sentence"

    test = Relations(relations)
    result = test.remove(sentence).strip()

    assert result == "his brother [[RELATION]] and sister [[RELATION]] <<NL>> Community Mental Health Team therefore this is a mikely test Community sentence"

  def test_relation_at_start_of_string(self, sentence1, relations):    
    test_sentence = "<<NL>>Mike has POA."
    test = Relations(relations)
    test.remove(sentence1)
    result = test.remove(test_sentence).strip()

    assert result == "<<NL>> [[RELATION]] has POA."

  def test_relation_at_mid_of_string(self, sentence1, relations):    
    test_sentence = "word.<<NL>>Mike has POA."
    test = Relations(relations)
    test.remove(sentence1)
    result = test.remove(test_sentence).strip()

    assert result == "word. <<NL>> [[RELATION]] has POA."
    