from redact.telephone import TelephoneNumber

class TestRedactPhoneNumber:
  def test_type_1(self):
    t = TelephoneNumber()
    r = t.remove("0131 537 0000")
    assert r == "[[TELEPHONE]]"

  def test_type_2(self):
    t = TelephoneNumber()
    r = t.remove("0131 537 0000")
    assert r == "[[TELEPHONE]]"

  def test_type_3(self):
    t = TelephoneNumber()
    r = t.remove("0131 537 0000")
    assert r == "[[TELEPHONE]]"
