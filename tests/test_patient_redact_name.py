from redact.patient import Patient

import pytest
import re

class TestPatientName:
  @pytest.fixture
  def patient_under_test(self) -> Patient:
    return Patient(name="Jane Elizabeth Doe")

  @pytest.fixture
  def sentence(self):
    return "Lorem ipsum dolor sit amet Jane Doe consectetur adipiscing elit"

  def test_redact_full_name(self, sentence, patient_under_test):
    r = patient_under_test.remove_name(sentence)

    assert "[[NAME]]" in r

  def test_redact_initial_and_lastname(self, sentence, patient_under_test):
    r = patient_under_test.remove_name(sentence.replace("Jane", "J"))

    assert "[[NAME]]" in r

  def test_redact_name_insensitive(self, sentence, patient_under_test):
    r = patient_under_test.remove_name(sentence.lower())

    assert "[[NAME]]" in r

  def test_redact_only_initial(self, sentence, patient_under_test):
    r = patient_under_test.remove_name(sentence)

    assert "Lorem" in r
    assert "elit" in r

  def test_redact_only_last_part(self, patient_under_test):
    sentence = "this is a test to make sure that Elizabeth Doe do and not get cotton etc"
    r = patient_under_test.remove_name(sentence)

    assert "[[NAME]]" in r

  def test_over_redaction(self, patient_under_test):
    sentence = "this is a test to make sure that xfon do and not get cotton etc"
    r = patient_under_test.remove_name(sentence)

    assert r == sentence
