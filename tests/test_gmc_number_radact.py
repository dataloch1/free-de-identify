import pytest
from redact.gmc_number import GMCNumber

class TestGMCNumberRedact:
  @pytest.fixture
  def sentence_with(self):
    def _sentence_with(gmc_no):
      return f"Lorem ipsum dolor sit amet {gmc_no} consectetur adipiscing elit"

    return _sentence_with

  def test_redact_format_1(self, sentence_with):
    g = GMCNumber()
    r = g.remove(sentence_with('GMC 123456789'))

    assert "[[GMC_NUMBER]" in r

  def test_redact_format_2(self, sentence_with):
    g = GMCNumber()
    r = g.remove(sentence_with('GMC: 123456789'))

    assert "[[GMC_NUMBER]" in r

  def test_redact_format_3(self, sentence_with):
    g = GMCNumber()
    r = g.remove(sentence_with('GMC No 123456789'))

    assert "[[GMC_NUMBER]" in r

  def test_redact_format_4(self, sentence_with):
    g = GMCNumber()
    r = g.remove(sentence_with('GMC NO 123456789'))

    assert "[[GMC_NUMBER]" in r