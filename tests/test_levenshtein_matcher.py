from matchers.levenshtein_matcher import LevenshteinMatcher

import pytest

class TestLevenshteinMatcher:
  @pytest.fixture
  def mc_name_var(self):
    return ["MCDuck", "MCDUCK", "MDUCK", "MCuck", "Mc Duck"]

  @pytest.fixture
  def mac_name_var(self):
    return ["MACDuck", "MACDUCK", "MaDuck", "McDuck", "Mac Duck", "Mc Duck"]

  def sentence(self, name):
    return f"Lorem ipsum dolor sit amet {name} consectetur adipiscing elit"

  @pytest.fixture
  def list_of_sentences_mc(self, mc_name_var):
    return map(self.sentence, mc_name_var)

  @pytest.fixture
  def list_of_sentences_mac(self, mac_name_var):
    return map(self.sentence, mac_name_var)
    
  def test_mc_variants(self, list_of_sentences_mc):
    for sentence in list_of_sentences_mc:
      tokens = sentence.split(' ')
      lt = LevenshteinMatcher(tokens)
      assert lt.find("McDuck") != -1

  def test_mac_variants(self, list_of_sentences_mac):
    for sentence in list_of_sentences_mac:
      tokens = sentence.split(' ')
      lt = LevenshteinMatcher(tokens)
      assert lt.find("MacDuck") != -1
