import pytest
from redact.healthcare_professional import HealthcareProfessional

class TestHealthcareProfessional:
  @pytest.fixture
  def ward_round(self):
    def _ward_round(name):
      return f"wR {name}"

    return _ward_round
  
  @pytest.fixture
  def names(self):
    return ["Paul Mitchell", "P Mitchell", "Paul D Mitchell", "Paul P D Mitchell", "P D Mitchell"]

  def test_wr_with_dr_first_and_sur(self, ward_round):
    c = HealthcareProfessional()
    result = c.remove(ward_round("Dr Paul Mitchell"))

    assert result == ward_round("[[HEALTHCARE_PROFESSIONAL]]")

  def test_wr_with_dr_period_first_and_sur(self, ward_round):
    c = HealthcareProfessional()
    result = c.remove(ward_round("Dr. Paul Mitchell"))

    assert result == ward_round("[[HEALTHCARE_PROFESSIONAL]]")

  def test_wr_with_dr_initial_and_sur(self, ward_round):
    c = HealthcareProfessional()
    result = c.remove(ward_round("Dr P Mitchell"))

    assert result == ward_round("[[HEALTHCARE_PROFESSIONAL]]")

  def test_wr_with_dr_period_initial_and_sur(self, ward_round):
    c = HealthcareProfessional()
    result = c.remove(ward_round("Dr. P Mitchell"))

    assert result == ward_round("[[HEALTHCARE_PROFESSIONAL]]")

  def test_wr_with_dr_and_sur(self, ward_round):
    c = HealthcareProfessional()
    result = c.remove(ward_round("Dr Mitchell"))

    assert result == ward_round("[[HEALTHCARE_PROFESSIONAL]]")

  def test_wr_with_dr_period_and_sur(self, ward_round):
    c = HealthcareProfessional()
    result = c.remove(ward_round("Dr. Mitchell"))

    assert result == ward_round("[[HEALTHCARE_PROFESSIONAL]]")

  def test_wr_with_first_and_sur(self, ward_round):
    c = HealthcareProfessional()
    result = c.remove(ward_round("Paul Mitchell"))

    assert result == ward_round("[[HEALTHCARE_PROFESSIONAL]]")

  def test_wr_with_initial_and_sur(self, ward_round):
    c = HealthcareProfessional()
    result = c.remove(ward_round("P Mitchell"))

    assert result == ward_round("[[HEALTHCARE_PROFESSIONAL]]")

  def test_wr_with_leading_quote(self, ward_round):
    c = HealthcareProfessional()
    result = c.remove(ward_round("\"P Mitchell"))

    assert result == ward_round("\"[[HEALTHCARE_PROFESSIONAL]]")

  def test_wr_with_new_line_marker(self, ward_round):
    c = HealthcareProfessional()
    result = c.remove(ward_round("<<NR>>P Mitchell"))

    assert result == ward_round("<<NR>>[[HEALTHCARE_PROFESSIONAL]]")

  def test_dr_with_names(self, names):
    c = HealthcareProfessional()
    for name in names:
      result = c.remove(f"Dr {name}")

      assert result == "[[HEALTHCARE_PROFESSIONAL]]"

  def test_mr_with_names(self, names):
    c = HealthcareProfessional()
    for name in names:
      result = c.remove(f"Mr {name}")

      assert result == "[[HEALTHCARE_PROFESSIONAL]]"

  def test_ms_with_names(self, names):
    c = HealthcareProfessional()
    for name in names:
      result = c.remove(f"Ms {name}")

      assert result == "[[HEALTHCARE_PROFESSIONAL]]"

  def test_sn_with_names(self, names):
    c = HealthcareProfessional()
    for name in names:
      result = c.remove(f"SN {name}")

      assert result == "[[HEALTHCARE_PROFESSIONAL]]"

    for name in names:
      result = c.remove(f"s/n {name}")

      assert result == "[[HEALTHCARE_PROFESSIONAL]]"

    for name in names:
      result = c.remove(f"s\\n {name}")

      assert result == "[[HEALTHCARE_PROFESSIONAL]]"

  def test_grades_with_name(self, names):
    c = HealthcareProfessional()
    for name in names:
      result = c.remove(f"{name} FY1")

      assert result == "[[HEALTHCARE_PROFESSIONAL]]"

    for name in names:
      result = c.remove(f"{name} (FY1)")

      assert result == "[[HEALTHCARE_PROFESSIONAL]]"

    for name in names:
      result = c.remove(f"{name} S/N")

      assert result == "[[HEALTHCARE_PROFESSIONAL]]"

    for name in names:
      result = c.remove(f"{name} (S/N)")

      assert result == "[[HEALTHCARE_PROFESSIONAL]]"

  def test_grades_with_name_nl_grade(self, names):
    c = HealthcareProfessional()
    for name in names:
      result = c.remove(f"{name}<<NL>>FY1")

      assert result == "[[HEALTHCARE_PROFESSIONAL]]<<NL>>FY1"

    for name in names:
      result = c.remove(f"{name}<<NL>>(FY1)")

      assert result == "[[HEALTHCARE_PROFESSIONAL]]<<NL>>(FY1)"

    for name in names:
      result = c.remove(f"{name}<<NL>>S/N")

      assert result == "[[HEALTHCARE_PROFESSIONAL]]<<NL>>S/N"

    for name in names:
      result = c.remove(f"{name}<<NL>>(S/N)")

      assert result == "[[HEALTHCARE_PROFESSIONAL]]<<NL>>(S/N)"

  def test_for_over_redaction_case_1(self):
    c = HealthcareProfessional()
    sentence = "Two films. Normal pulmonary vascularity."

    result = c.remove(sentence)

    assert result == sentence

  def test_for_over_redaction_cas_2(self):
    c = HealthcareProfessional()
    sentence = "Pneumothorax Right"

    result = c.remove(sentence)

    assert result == sentence

