from redact.eref_letter import ErefLetter
import pytest

class TestErefLetter:
  def test_end_yours_sincerely(self):
    sentence = "Thanks you for your advice. Yours sincerely, Dr P Mitchell"
    expected = "Thanks you for your advice. Yours sincerely[[HEALTHCARE_PROFESSIONAL]]"

    e = ErefLetter()
    r = e.remove(sentence)

    assert r == expected

  def test_end_yours_faithfully(self):
    sentence = "Thanks you for your advice. Yours faithfully, Dr P Mitchell"
    expected = "Thanks you for your advice. Yours faithfully[[HEALTHCARE_PROFESSIONAL]]"

    e = ErefLetter()
    r = e.remove(sentence)

    assert r == expected

  def test_referral_from(self):
    sentence = "Referrals Tab *<<NL>><<NL>>Referral from: Dr P Mitchell additional stuff<<NL>><<NL>>Main referral text:"
    expected = "Referrals Tab *<<NL>><<NL>>Referral from: [[HEALTHCARE_PROFESSIONAL]]<<NL>><<NL>>Main referral text:"

    e = ErefLetter()
    r = e.remove(sentence)

    assert r == expected
