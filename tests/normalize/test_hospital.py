from normalize.hospital_names import HospitalNames
import pytest

class TestHospital:
  @pytest.fixture
  def sentence(self):
    def _sentence(hospital_name):
      return f"This is a {hospital_name} Hospital name"

    return _sentence

  def test_hospital_name_1(self, sentence):
    h = HospitalNames()
    r = h.normalize(sentence("St Johns"))

    assert "SJH" in r

  def test_hospital_name_2(self, sentence):
    h = HospitalNames()
    r = h.normalize(sentence("St Michaels"))

    assert "SMH" in r

