import io
from document import Document

def test_load_document():
  doc = Document("tests/fixtures/document.txt")
  assert doc.load() == True

def test_stream_document():
  doc = Document("tests/fixtures/document.txt")
  doc.load()
  assert type(doc.steam()) is io.FileIO
  