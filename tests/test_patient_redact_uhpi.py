from datetime import datetime
from redact.patient import Patient

import pytest
import re

class TestPatientUHPI:
  @pytest.fixture
  def patient_under_test(self) -> Patient:
    return Patient(name="Jane Elizabeth Doe", uhpi='500123456A')

  @pytest.fixture
  def sentence_with(self):
    def _sentence_with(uhpi):
      return f"Lorem ipsum dolor sit amet {uhpi} consectetur adipiscing elit"

    return _sentence_with

  def test_with_no_uhpi(self, sentence_with, patient_under_test):
    r = patient_under_test.remove_uhpi(sentence_with('any'))

    assert r == sentence_with('any')

  def test_with_a_uhpi(self, sentence_with, patient_under_test):
    r = patient_under_test.remove_uhpi(sentence_with('500123456A'))

    assert "[[UHPI]]" in r

  def test_with_unknown_uhpi(self, sentence_with, patient_under_test):
    r = patient_under_test.remove_uhpi(sentence_with('500123456A, 700123456A'))
    
    assert "[[UHPI]], [[SUSPICIOUS_NUMBER]]" in r
    