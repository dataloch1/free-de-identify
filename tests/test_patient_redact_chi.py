from datetime import datetime
from redact.patient import Patient

import pytest
import re

class TestPatientCHI:
  @pytest.fixture
  def sentence_with(self):
    def _sentence_with(chi):
      return f"Lorem ipsum dolor sit amet {chi} consectetur adipiscing elit"

    return _sentence_with

  @pytest.fixture
  def valid_chi(self) -> str:
    return "0102034567"

  @pytest.fixture
  def other_chi(self) -> str:
    return "0203045678"

  @pytest.fixture
  def suspicious_number(self) -> str:
    return "1234567890"

  @pytest.fixture
  def dob_1(self) -> datetime:
    return datetime(year=1903, month=2, day=1)

  def test_redact_chi(self, sentence_with, valid_chi, dob_1):
    p = Patient(name="Jane Doe", dob=dob_1)
    r = p.remove_chis(sentence_with(valid_chi))

    assert "[[CHI]]" in r

  def test_redact_other_chi(self, sentence_with, other_chi, dob_1):
    p = Patient(name="Jane Doe", dob=dob_1)
    r = p.remove_chis(sentence_with(other_chi))

    assert "[[OTHER_CHI]]" in r

  def test_redact_suspicious(self, sentence_with, suspicious_number, dob_1):
    p = Patient(name="Jane Doe", dob=dob_1)
    r = p.remove_chis(sentence_with(suspicious_number))

    assert "[[SUSPICIOUS_NUMBER]]" in r
