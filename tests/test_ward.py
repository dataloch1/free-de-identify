from redact.ward import Ward
import pytest

class TestWard:
  def test_number_ward_1(self):
    w = Ward()
    r = w.remove("Ward: 123")

    assert "[[WARD]]" in r

  def test_number_ward_2(self):
    w = Ward()
    r = w.remove("Ward 123")

    assert "[[WARD]]" in r

  def test_name_ward(self):
    w = Ward()
    r = w.remove("Rowan ward")

    assert "[[WARD]]" in r

  def test_name_wd(self):
    w = Ward()
    r = w.remove("Wd 45")

    assert "[[WARD]]" in r
