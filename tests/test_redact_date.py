from datetime import date
from redact.date import Date as RedactDate

import pytest

class TestRedactDate:
  @pytest.fixture
  def dob(self):
    return date(year=2000, month=1, day=1)

  @pytest.fixture
  def sentence(self, dob):
    str = "The Patient's date of birth is "
    def _sentence(format):
      return str + dob.strftime(format) + str
  
    return _sentence

  def test_date_format_1(self, dob, sentence):
    p = RedactDate(dob, sub="[[DOB]]")
    r = p.remove(sentence("%d/%m/%Y"))
    assert "[[DOB]]" in r

  def test_date_format_2(self, dob, sentence):
    p = RedactDate(dob, sub="[[DOB]]")
    r = p.remove(sentence("%d.%m.%Y"))
    assert "[[DOB]]" in r

  def test_date_format_3(self, dob, sentence):
    p = RedactDate(dob, sub="[[DOB]]")
    r = p.remove(sentence("%d-%m-%Y"))
    assert "[[DOB]]" in r

  def test_date_format_4(self, dob, sentence):
    p = RedactDate(dob, sub="[[DOB]]")
    r = p.remove(sentence("%d%m%Y"))
    assert "[[DOB]]" in r

  def test_date_format_5(self, dob, sentence):
    p = RedactDate(dob, sub="[[DOB]]")
    r = p.remove(sentence("%d/%m/%y"))
    assert "[[DOB]]" in r

  def test_date_format_6(self, dob, sentence):
    p = RedactDate(dob, sub="[[DOB]]")
    r = p.remove(sentence("%d.%m.%y"))
    assert "[[DOB]]" in r

  def test_date_format_7(self, dob, sentence):
    p = RedactDate(dob, sub="[[DOB]]")
    r = p.remove(sentence("%d-%m-%y"))
    assert "[[DOB]]" in r

  def test_date_format_8(self, dob, sentence):
    p = RedactDate(dob, sub="[[DOB]]")
    r = p.remove(sentence("%d%m%y"))
    assert "[[DOB]]" in r

  def test_date_format_9(self, sentence):
    d = date(year=2000, month=1, day=1)
    p = RedactDate(d)
    r = p.remove(sentence("%d/%m/%Y"))
    assert "[[DATE]]" in r

  def test_date_not_date_of_interest1(self, sentence):
    d = date(year=2000, month=1, day=1)
    p = RedactDate(date.today())
    r = p.remove(sentence("%d/%m/%Y"))
    assert "[[DATE]]" not in r  

  def test_date_not_date_of_interest1(self, sentence):
    d = date(year=2000, month=1, day=1)
    p = RedactDate(date.today())
    r = p.remove(sentence("%d%m%Y"))
    assert "[[DATE]]" not in r  
   


