from io import SEEK_SET, StringIO
from operator import truediv

class Buffer:
  def __init__(self) -> None:
    self.file = StringIO()

  def load_from_file(self, io_stream):
    bytes_buffer = io_stream.read()
    string = bytes_buffer.decode('utf-8')
    self.file.write(string)
    
  def buffer(self):
    self.file.seek(0, SEEK_SET)
    return self.file.read()
